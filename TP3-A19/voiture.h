#ifndef __VOITURE__
#define __VOITURE__

#include "moteur.h"

class Voiture {
	private:
        Moteur* le_moteur;
        char id;
	public:
        Voiture(char id);
        void ajouterMoteur(Moteur* un_moteur);
        void enleverMoteur();
        Moteur* getMoteur();
        char getID();
};

#endif
